# **Testing exercise**

### Exercise to test a knowledge of Javascript,HTML,LESS,CSS,AngularJS,PHP,MySQL

## Instruction:
1. Clone project
2. Fix all mistakes if needed
3. Follow comments inside files
4. Push and commit your version of exercise
or provide info about how to download your modified project

## Do not use frameworks except jQuery and/or AngularJS (they're included)